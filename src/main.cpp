/*
* Projekt na kurs "Systemy operacyjne 2"
* Diamond rush - gra polegająca na zbieraniu diamentów
* Autorzy:
* + Dawid Leśniewski 
* + Zbigniew Bocianowski
* Data: 10.05.2018r. 
*/

#include "enemy.h"
#include "gameObject.h"
#include "player.h"
#include "screen.h"

#include <curses.h>
#include <random>
#include <sstream>
#include <time.h>
#include <unistd.h>
#include <vector>
#include <mutex>

int state = 0;   // 0 - menu, 1 - level, 2 - game over, 3 - win <3
int version = 0; // 0 - hidden, 1 - normal
bool gameRunning = true;

void stateControler()
{  
  while (gameRunning)
  {
    if (state == 0 || state == 2)
    {
      char input = getch();
      if (input == '1')
      {
        state = 1;
        version = 1;
      }
      else if (input == '2')
      {
        state = 1;
        version = 0;
      }
      else if (input == '3')
      {
        state = 3;
      }
    }
  }
}

int main()
{
  std::mutex mainMutex;
  initscr(); // Initializing ncurses

  curs_set(0);       // Disable cursor
  noecho();          // Diseable echoing of typed character form getch()
  srand(time(NULL)); // set seed for rand()

  thread input(stateControler); // Start of state controlling thread

  Screen scr;
  WINDOW *game = newwin(scr.getWidth() - 3, scr.getHeight(), 0, 0);
  WINDOW *score = newwin(3, scr.getHeight(), scr.getWidth() - 3, 0);

  // Main menu display ----------------------------------------------------------------

  while (state == 0)
  {
    scr.printMenu(game);
    wrefresh(game);
    wrefresh(score);
    napms(100);
  }

  // Creating map ---------------------------------------------------------------------

  int diamonds = 5; // numer of diamonds to pick
  int enemies = 5; // number of enemies

  scr.makeMap(game, diamonds, version == 1);

  int x_max, y_max; // Screen dimensions
  getmaxyx(game, y_max, x_max);

  player p(y_max / 2, x_max / 2, &scr, &mainMutex); // Creating player

  std::vector<enemy *> enemiesList; // Array of enemies
  int enemiesAdded = 0;
  while (enemiesAdded < enemies)
  {
    int x = rand() % x_max;
    int y = rand() % y_max;
    if (scr.checkPassage(x, y))
    {
      enemy *e = new enemy(y, x, &p, &scr, &mainMutex);
      enemiesList.push_back(e);
      enemiesAdded++;
    }
  }
  // Game loop ----------------------------------------------------------------------
  while (state == 1)
  {
    scr.drawBorders(game); /// Draw borders for views
    scr.drawBorders(score);    

    scr.drawMap(game, p.y, p.x); // Adds visible elements to map array for player position


    if (version == 1) // If in hidden mode draws "fog"
    {
      scr.drawFog(game);
    }

    // Refreshing score
    scr.refreshScore(score, 1, p.hp, enemies, p.diamonds, diamonds);

    // Checking players hp
    if (p.hp < 1)
    {
      erase();
      state = 2;
    }
    // Checking win condition 
    if (p.diamonds == diamonds)
    {
      erase();
      state = 3;
    }

    wrefresh(game); // Refreshing view
    wrefresh(score);

    napms(100); // Delay => refresh speed 10 fps
  }

  // Game over screen ------------------------------------------------------

  if (state == 2)
  {
    werase(game);
    werase(score);
    scr.printGameOver(game, y_max / 2, x_max / 2);
    wrefresh(game);
    napms(1000);
  }

  // Victory screen --------------------------------------------------------

  if (state == 3)
  {
    werase(game);
    werase(score);
    scr.printVictory(game, y_max / 2, x_max / 2);
    wrefresh(game);
    napms(1000);
  }

  delwin(game);
  delwin(score);
  endwin();
  gameRunning = false;
  input.join();

  return 0;
}
