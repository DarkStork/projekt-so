#include <string>
#include <chrono>
#include <iostream>
#include <ncurses.h>
#include "player.h"
#include "screen.h"

using namespace std;
using namespace std::chrono;
using namespace std::this_thread;

player::player(int startX, int startY, Screen *mapPointer, mutex* m)
{
    x = startX;
    y = startY;
    mtx = m;
    diamonds = 0;
    hp = 3;
    map = mapPointer;
    keypad(stdscr, TRUE);
    process = thread(&player::update, this);
}
player::~player()
{
    process.join();
}

char player::draw()
{
    char sign = '0';
    return sign;
}

void player::update()

{
    map->printGameObject(this, draw());
    while (1)
    {
        napms(100);
       
        char tileType = map->getTileType(y, x);
        if (tileType == 'v')
        {
            diamonds++;
        }
       
        int c = getch();

         mtx->lock();
         map->printGameObject(this, ' ');
         
        switch (c)
        {
        case KEY_DOWN:
            if (map->checkPassage(y, x + 1))
            {
                x++;
            }
            break;
        case KEY_UP:
            if (map->checkPassage(y, x - 1))
            {
                x--;
            }
            break;
        case KEY_LEFT:
            if (map->checkPassage(y - 1, x))
            {
                y--;
            }

            break;
        case KEY_RIGHT:
            if (map->checkPassage(y + 1, x))
            {
                y++;
            }
            break;
        }
        flushinp();
        map->printGameObject(this, draw());
        mtx->unlock();
    }
}

void player::damage(int dmg)
{
    hp -= dmg;
    if (hp < 0)
    {
        hp = 0;
    }
}
