#include <string.h>
#include "player.h"
#include "gameObject.h"
#include "screen.h"

using namespace std;

class enemy : public gameObject
{
public:
  player *target;
  Screen *map;
  char draw();
  void update();
  enemy(int y, int x, player *player, Screen *mapPointer, mutex* m);
  ~enemy();
};