#include "screen.h"
#include <cstdlib>
#include <ctime>
#include <numeric>
#include <random>
#include <sstream>
#include <string>
#include <vector>

using std::vector;

Screen::Screen() { getmaxyx(stdscr, screen_width, screen_height); }

Screen::~Screen() {}

int Screen::getHeight() { return screen_height; }

int Screen::getWidth() { return screen_width; }

void Screen::printGameObject(gameObject * object, char sign){
   actualMapTable[object->y][object->x] = sign;
   gameStateTable[object->y][object->x] = sign;
   
}

void Screen::drawBorders(WINDOW *window)
{
  int x, y;
  getmaxyx(window, y, x);
  mvwprintw(window, 0, 0, "+");
  mvwprintw(window, y - 1, 0, "+");
  mvwprintw(window, 0, x - 1, "+");
  mvwprintw(window, y - 1, x - 1, "+");

  for (int i = 1; i < (y - 1); i++)
  {
    mvwprintw(window, i, 0, "|");
    mvwprintw(window, i, x - 1, "|");
  }

  for (int i = 1; i < (x - 1); i++)
  {
    mvwprintw(window, 0, i, "-");
    mvwprintw(window, y - 1, i, "-");
  }
}

void Screen::makeMap(WINDOW *window, int diamonds, bool showAll)
{

  int x_max, y_max;
  getmaxyx(window, y_max, x_max);
  std::string line = "";

  for (int i = 0; i < x_max; i++)
  {
    line += "+";
  }

  for (int i = 0; i < x_max; i++)
  {
    actualMapTable.push_back(std::string(line));
  }

  for (int x = 0; x < x_max; x++)
  {
    for (int y = 0; y < y_max; y++)
    {
      actualMapTable[x_max / 2][y] = ' ';
    }
    actualMapTable[x][y_max / 2] = ' ';
  }

  std::vector<std::pair<int, int>> drillers;

  drillers.push_back(std::make_pair(x_max / 2, 1));         // top
  drillers.push_back(std::make_pair(x_max - 2, y_max / 2)); // right
  drillers.push_back(std::make_pair(x_max / 2, y_max - 2)); // bottom
  drillers.push_back(std::make_pair(1, y_max / 2));         // left
  int fields = (y_max - 2) * (x_max - 2);
  int empty_fields = (y_max - 2) + (x_max - 2);

  std::mt19937 rng;
  rng.seed(std::random_device()());
  std::uniform_int_distribution<std::mt19937::result_type> distr(1, 10000000);

  while (empty_fields < 0.65 * fields)
  {
    for (int driller_numer = 0; driller_numer < drillers.size();
         driller_numer++)
    {
      for (int i = 0; i < 10; i++)
      {

        switch (distr(rng) % 4)
        {
        case 0: // moving top
          if (drillers[driller_numer].second > 2)
          {
            drillers[driller_numer].second -= 1;
            if (actualMapTable[drillers[driller_numer].first]
                              [drillers[driller_numer].second] != ' ')
            {
              empty_fields++;
            }
            actualMapTable[drillers[driller_numer].first]
                          [drillers[driller_numer].second] = ' ';
          }
          break;

        case 1: // moving right
          if (drillers[driller_numer].first < x_max - 2)
          {
            drillers[driller_numer].first += 1;
            if (actualMapTable[drillers[driller_numer].first]
                              [drillers[driller_numer].second] != ' ')
            {
              empty_fields++;
            }
            actualMapTable[drillers[driller_numer].first]
                          [drillers[driller_numer].second] = ' ';
          }
          break;

        case 2: // moving bot
          if (drillers[driller_numer].second < y_max - 2)
          {
            drillers[driller_numer].second += 1;
            if (actualMapTable[drillers[driller_numer].first]
                              [drillers[driller_numer].second] != ' ')
            {
              empty_fields++;
            }
            actualMapTable[drillers[driller_numer].first]
                          [drillers[driller_numer].second] = ' ';
          }
          break;

        case 3: // moving left
          if (drillers[driller_numer].first > 2)
          {
            drillers[driller_numer].first -= 1;
            if (actualMapTable[drillers[driller_numer].first]
                              [drillers[driller_numer].second] != ' ')
            {
              empty_fields++;
            }
            actualMapTable[drillers[driller_numer].first]
                          [drillers[driller_numer].second] = ' ';
          }
          break;
        }
      }
    }
  }

  // Adding diamonds
  int diamondsPlaced = 0;
  while (diamondsPlaced < diamonds)
  {
    int y = distr(rng) % y_max;
    int x = distr(rng) % x_max;
    char symbol = actualMapTable[x][y];

    if (symbol == ' ')
    {
      actualMapTable[x][y] = 'v';
      diamondsPlaced++;
    }
  }

  // Filling game state with X

  std::string screenLine = "";

  for (int i = 0; i < x_max; i++)
  {
    screenLine += "X";
  }

  for (int i = 0; i < x_max; i++)
  {
    gameStateTable.push_back(std::string(screenLine));
  }

  if (showAll)
  {
    for (int y = 0; y < y_max; y++)
    {
      for (int x = 0; x < x_max; x++)
      {
        gameStateTable[x][y] = actualMapTable[x][y];
      }
    }
  }
}

void Screen::drawMap(WINDOW *window, int x_pos, int y_pos)
{

  int x_max, y_max;
  char character;
  getmaxyx(window, y_max, x_max);

  char currentTile = actualMapTable[x_pos][y_pos];
  // if (currentTile == 'v')
  // {
  //   actualMapTable[x_pos][y_pos] = ' ';
  // }
  
  // printing on left
  if (x_pos > 2)
  {
    character = actualMapTable[x_pos - 2][y_pos];
    gameStateTable[x_pos - 2][y_pos] = character;
  }
  if (x_pos > 1)
  {
    character = actualMapTable[x_pos - 1][y_pos];
    gameStateTable[x_pos - 1][y_pos] = character;
  }

  // printing on right
  if (x_pos < x_max - 3)
  {
    character = actualMapTable[x_pos + 2][y_pos];
    gameStateTable[x_pos + 2][y_pos] = character;
  }
  if (x_pos < x_max - 2)
  {
    character = actualMapTable[x_pos + 1][y_pos];
    gameStateTable[x_pos + 1][y_pos] = character;
  }

  // printing on top
  if (y_pos > 2)
  {
    character = actualMapTable[x_pos][y_pos - 2];
    gameStateTable[x_pos][y_pos - 2] = character;
  }
  if (y_pos > 1)
  {
    character = actualMapTable[x_pos][y_pos - 1];
    gameStateTable[x_pos][y_pos - 1] = character;
  }

  // printing on bottom
  if (y_pos < y_max - 3)
  {
    character = actualMapTable[x_pos][y_pos + 2];
    gameStateTable[x_pos][y_pos + 2] = character;
  }
  if (y_pos < y_max - 2)
  {
    character = actualMapTable[x_pos][y_pos + 1];
    gameStateTable[x_pos][y_pos + 1] = character;
  }

  // printing on left top
  if (x_pos > 1 && y_pos > 1)
  {
    character = actualMapTable[x_pos - 1][y_pos - 1];
    gameStateTable[x_pos - 1][y_pos - 1] = character;
  }
  // printing on right top
  if (x_pos < x_max - 2 && y_pos > 1)
  {
    character = actualMapTable[x_pos + 1][y_pos - 1];
    gameStateTable[x_pos + 1][y_pos - 1] = character;
  }
  // printing on left bottom
  if (x_pos > 1 && y_pos < y_max - 2)
  {
    character = actualMapTable[x_pos - 1][y_pos + 1];
    gameStateTable[x_pos - 1][y_pos + 1] = character;
  }
  // printing on right bottom
  if (x_pos < x_max - 2 && y_pos < y_max - 2)
  {
    character = actualMapTable[x_pos + 1][y_pos + 1];
    gameStateTable[x_pos + 1][y_pos + 1] = character;
  }

  gameStateTable[x_pos][y_pos] = actualMapTable[x_pos][y_pos];

  checkWholeMap(window);
}

void Screen::checkWholeMap(WINDOW *window)
{

  int x_max, y_max;
  std::string line = "";
  getmaxyx(window, y_max, x_max);

  for (int x = 1; x < x_max - 1; x++)
  {
    for (int y = 1; y < y_max - 1; y++)
    {
      line = gameStateTable[x][y];
      mvwprintw(window, y, x, line.c_str());
    }
  }
}

void Screen::drawFog(WINDOW *window)
{

  int x_max, y_max;
  std::string line = "";
  getmaxyx(window, y_max, x_max);

  for (int x = 1; x < x_max - 1; x++)
  {
    for (int y = 1; y < y_max - 1; y++)
    {
      line = gameStateTable[x][y];
      if (line == "X")
      {
        mvwprintw(window, y, x, line.c_str());
      }
    }
  }
}

void Screen::refreshScore(WINDOW *window, int level, int hp, int enemies, int diamonds,
                          int max_diamonds)
{

  std::stringstream buffer;
  buffer << "Level: " << level << "     HP: " << hp << " <3     Enemies: " << enemies
         << "      Diamonds: " << diamonds << "/" << max_diamonds;
  attron(A_BOLD);
  mvwprintw(window, 1, 1, buffer.str().c_str());
  attroff(A_BOLD);
}

bool Screen::checkPassage(int y, int x)
{
  char symbol = actualMapTable[y][x];
  return (symbol != '+');
}

char Screen::getTileType(int y, int x)
{
  char symbol = actualMapTable[y][x];
  return (symbol);
}

void Screen::printTitle(WINDOW *window, int y, int x)
{
  std::string line1 = "   ___  _                         __  ___           __  \n";
  std::string line2 = "  / _ \\(_)__ ___ _  ___  ___  ___/ / / _ \\__ _____ / /  \n";
  std::string line3 = " / // / / _ `/  ' \\/ _ \\/ _ \\/ _  / / , _/ // (_-</ _ \\ \n";
  std::string line4 = "/____/_/\\_,_/_/_/_/\\___/_//_/\\_,_/ /_/|_|\\_,_/___/_//_/ \n";
  mvwprintw(window, y, x, line1.c_str());
  mvwprintw(window, y + 1, x, line2.c_str());
  mvwprintw(window, y + 2, x, line3.c_str());
  mvwprintw(window, y + 3, x, line4.c_str());
}

void Screen::printGameOver(WINDOW *window, int y, int x)
{
  std::string line1 = "  ________   __  _______  ____ _   _________";
  std::string line2 = " / ___/ _ | /  |/  / __/ / __ \\ | / / __/ _ \\";
  std::string line3 = "/ (_ / __ |/ /|_/ / _/  / /_/ / |/ / _// , _/";
  std::string line4 = "\\___/_/ |_/_/  /_/___/  \\____/|___/___/_/|_| ";
  mvwprintw(window, y, (x - line1.length() / 2), line1.c_str());
  mvwprintw(window, y + 1, (x - line2.length() / 2), line2.c_str());
  mvwprintw(window, y + 2, (x - line3.length() / 2), line3.c_str());
  mvwprintw(window, y + 3, (x - line4.length() / 2), line4.c_str());
}

void Screen::printVictory(WINDOW *window, int y, int x)
{
  std::string line1 = "  _   _____________________  _____  __";
  std::string line2 = " | | / /  _/ ___/_  __/ __ \\/ _ \\ \\/ /";
  std::string line3 = " | |/ // // /__  / / / /_/ / , _/\\  / ";
  std::string line4 = " |___/___/\\___/ /_/  \\____/_/|_| /_/  ";
  mvwprintw(window, y, (x - line1.length() / 2), line1.c_str());
  mvwprintw(window, y + 1, (x - line2.length() / 2), line2.c_str());
  mvwprintw(window, y + 2, (x - line3.length() / 2), line3.c_str());
  mvwprintw(window, y + 3, (x - line4.length() / 2), line4.c_str());
}

void Screen::printMenu(WINDOW *window)
{
  int maxX, maxY;
  clear();
  getmaxyx(window, maxY, maxX);
  printTitle(window, maxY / 2 - 15, maxX / 2 - 30);
  std::string normal = "1. Normal mode";
  std::string hidden = "2. Hidden mode";
  std::string exit = "3. Exit";

  mvwprintw(window, maxY / 2 - 9, maxX / 2 - 10, normal.c_str());
  mvwprintw(window, maxY / 2 - 6, maxX / 2 - 10, hidden.c_str());
  mvwprintw(window, maxY / 2 - 3, maxX / 2 - 6, exit.c_str());
}
