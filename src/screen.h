#include <chrono>
#include <iostream>
#include <ncurses.h>
#include <numeric>
#include <thread>
#include <string>
#include <vector>

#include "gameObject.h"

#pragma once



class Screen
{

private:
  int screen_width;
  int screen_height;

public:
  Screen();
  ~Screen();
  int getWidth();
  int getHeight();
  void drawBorders(WINDOW *window);
  void drawMap(WINDOW *window, int x_pos, int y_pos);
  void drawFog(WINDOW *window);
  void makeMap(WINDOW *window, int diamonds, bool showAll);
  std::vector<std::string> gameStateTable;                                          
  std::vector<std::string> actualMapTable; 
  void checkWholeMap(WINDOW *window);
  void refreshScore(WINDOW *window, int level, int hp, int enemies, int diamonds, int max_diamonds);
  bool checkPassage(int y, int x);
  char getTileType(int y, int x);
  void printTitle(WINDOW *window, int y, int x);
  void printMenu(WINDOW *window);
  void printGameOver(WINDOW *window, int y, int x);
  void printVictory(WINDOW *window, int y, int x);
  void printGameObject(gameObject* object, char sign);
};
