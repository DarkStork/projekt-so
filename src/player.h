#include <string.h>
#include "gameObject.h"
#include "screen.h"

using namespace std;

#pragma once

class player : public gameObject
{
private:
  Screen *map;
public:
  int diamonds;
  int hp;
  char draw();
  void update();
  void damage(int dmg);
  player(int x, int y, Screen *map, mutex* m);
  ~player();
};