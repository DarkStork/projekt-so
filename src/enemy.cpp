#include <string>
#include <ncurses.h>

#include "enemy.h"
#include "player.h"

enemy::enemy(int startY, int startX, player *p, Screen *mapPointer, mutex* m)
{
    x = startY;
    y = startX;
    mtx = m;
    map = mapPointer;
    target = p;
    keypad(stdscr, TRUE);
    process = thread(&enemy::update, this);
}

enemy::~enemy()
{
    process.join();
}

char enemy::draw()
{
    char sign = 'E';
    return sign;
}

void enemy::update()
{
    while (1)
    {
        napms(300);
        mtx->lock();
        map->printGameObject(this,' ');
        if (target->x == x && target->y == y)
        {
            target->damage(1);
        }        
        
        int dist = (target->x - x) * (target->x - x) + (target->y - y) * (target->y - y);
        bool chase = false;
        if (dist < 200)
        {
            chase = true;
        }
        if (chase)
        {
            if (dist > 500)
            {
                chase = false;
            }
            if (target->x == x && target->y == y)
            {
                target->damage(1);
            }
            if (target->x > x)
            {
                if (map->checkPassage(y, x + 1))
                {
                    x++;
                }
            }
            else if (target->x < x)
            {
                if (map->checkPassage(y, x - 1))
                {
                    x--;
                }
            }

            if (target->y > y)
            {
                if (map->checkPassage(y + 1, x))
                {
                    y++;
                }
            }
            else if (target->y < y)
            {
                if (map->checkPassage(y - 1, x))
                {
                    y--;
                }
            }
        }
        map->printGameObject(this,draw());
        mtx->unlock();
    }
}
