
#pragma once
// Base object for any objects in the game like the player and enemies
#include <string.h>
#include <thread>
#include <mutex>

using namespace std;

class gameObject
{
public: // global public static
  thread process;
  mutex *mtx;
  int x;
  int y;
  virtual char draw(){
    return 'F';
  };
  virtual void update() = 0;
  gameObject();
  ~gameObject();
};