# Projekt z Systemów operacyjnych 2
## Odkryj mapę i znajdź diamenty - prosta gra w terminalu

Celem projektu jest stworzenie prostej gry z wykorzystaniem biblioteki ncurses i wątków pod systemem operacyjnym Linux. Gra polega na poruszaniu się po mapie bohaterem i zbieraniu określonej ilości diamentów. Aby utrudnić to zadanie, mapa jest początkowo nieznana i widać tylko pola w określonej odległości od gracza. Poruszając się, gracz odkrywa nowe pola, dzięki czemu widzi, gdzie są przeszkody i diamenty.Aby zapewnić graczowi wyzwanie w grze będą także przeciwnicy goniący gracza, którzy będą od niego wolniejsi, ale dzięki przeszkodom i przewadze liczebnej będą zwiększali trudność rozgrywki.

### Skład grupy projektowej:
+ Dawid Leśniewski 226090
+ Zbigniew Bocianowski 226054
